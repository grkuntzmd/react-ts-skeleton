# Using this skeleton

- Clone the git repository.
- Install a recent version of nodejs.
- Run `npm install`.
- Modify `src/app.tsx` to your heart's content.
- Test with `npm start` and open your browser to `http://localhost:8080/`.